# Easy Assembly - Ui and Motor control

## Description

In this repository is a UI and driver ment for the UstepperS used in the Easy Assembly eksamens project as part of IT-teknology.

This is very much a prototype and not ment for production use.

## Changelog

25/05/2022

- Made changes to the motor code for the uStepper, will update log with more info later.

- Made muliple updates and added features to the UI, will update log with more info later.

23/05/2022

- Added automatic refresh functionality for the port select

18/05/2022

- Added Serial read terminal to the UI

- Added Serial printouts from the motor

- Cleaned up the Motor driver to remove unused functions
