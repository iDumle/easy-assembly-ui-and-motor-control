#include <uStepperS.h>

// Variables

uStepperS uStep;

const int MaxAcc = 2000;
const int MaxVel = 800;

#define INPUT_SIZE 30 // Calculate based on max input size expected for one command

// Main Arduino Functions

void setup(void) {
    Serial.begin(115200);
    Serial.println("uStepper | Device connected to uStepper");
    uStep.setup();
    uStep.checkOrientation(30.0);       //Check orientation of motor connector with +/- 30 microsteps movement
    uStep.setControlThreshold(15);
    uStep.setMaxAcceleration(MaxAcc);
    uStep.setMaxVelocity(MaxVel);
}

void loop(void) {
    while(!Serial.available()) {
        readSerialInput();
    }
}

// Functions

void moveStepCCW(int32_t step_value){
    Serial.print("uStepper | Moving ");
    Serial.print(step_value);
    Serial.println(" Steps Counter Clockwise.");
    uStep.moveSteps(step_value);
}

void moveStepCW(int32_t step_value){
    Serial.print("uStepper | Moving ");
    Serial.print(step_value);
    Serial.println(" Steps Clockwise.");
    uStep.moveSteps(step_value);
}

void moveConstantCCW(int rpm_value){
    Serial.print("uStepper | Moving continuesly counter clockwise at: ");
    Serial.print(rpm_value);
    Serial.println(" RPM.");
    uStep.setRPM(rpm_value);
}

void moveConstantCW(int rpm_value){
    Serial.print("uStepper | Moving continuesly clockwise at: ");
    Serial.print(rpm_value);
    Serial.println(" RPM.");
    uStep.setRPM(rpm_value);
}

void readSerialInput(){
    // Get next command from Serial (add 1 for final 0)
    char input[INPUT_SIZE + 1];
    byte size = Serial.readBytes(input, INPUT_SIZE);
    // Add the final 0 to end the C string
    input[size] = 0; 

    // Read each command pair 
    char* command = strtok(input, "&");
    while (command != 0) {
        // Split the command in two values
        char* separator = strchr(command, ':');
        if (separator != 0) {
            // Actually split the string in 2: replace ':' with 0
            *separator = 0;
            int cmd = atoi(command);
            ++separator;
            int32_t packet = atol(separator);

            if(cmd == 1){
                uStep.stop();
                Serial.println("uStepper | Stopping The Motor!");
            } else if (cmd == 2){
                moveConstantCCW(-packet);
            } else if(cmd == 3){
                moveConstantCW(packet);
            } else if (cmd == 4){
                moveStepCCW(-packet);
            } else if(cmd == 5){
                moveStepCW(packet);
            } 
        }
        // Find the next command in input string
        command = strtok(0, "&");
    }
}
