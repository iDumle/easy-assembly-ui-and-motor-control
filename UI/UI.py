import tkinter as tk
import serial
import serial.tools.list_ports

"""Variables"""


#Serial Ports found
port_list = [" "]
ports = [" "]

#Serial settings
ser = serial.Serial()
ser_baudrate = 115200

#Canvas list
canvas_list = ["# Move Step feature is locked to 200 RPM.","- Starting Program -"]

"""Functions"""

def refresh_serial():
    global port_list
    global ports
    menu = serial_devices["menu"]
    port_list.clear()
    ports.clear()

    port_list = [tuple(p) for p in list(serial.tools.list_ports.comports())]

    for eachPort in port_list:
        ports.append(eachPort[0])

    menu.delete(0,"end")
    for string in ports:
        menu.add_command(label=string, command=lambda value=string: selected_port.set(value))
    

def serial_Connect():
    com_port = selected_port.get()
    if com_port == "Select a Port":
        canvas_list.append("Please Select a Port to Connect to!")
    else:
        ser.port = com_port
        ser.baudrate = ser_baudrate
        try:
            ser.open()
        except:
            ser.close()
            ser.open()
        msg = "Connecting to serial port: "+com_port
        canvas_list.append(msg)

def serial_Disconnect():
    ser.close()
    canvas_list.append("Diconnecting from serial.")

#Serial code
def serial_Write(value1,value2=0):
    try:
        str_val1 = str(value1)
        str_val2 = str(value2)
        byte_val1 = str_val1.encode()
        byte_val2 = str_val2.encode()
        payload = byte_val1+b':'+byte_val2
        ser.write(payload)
    except:
        canvas_list.append("Please connect to a serial device!")

def listen_Serial_port():
        if ser.isOpen() and ser.in_waiting:
            recentPacket = ser.readline()
            recentPacketString = recentPacket.decode("utf").rstrip('\n')
            canvas_list.append(recentPacketString)

def display_list():
        column = 0
        row = 0

        for widget in sic_frame.winfo_children():
            widget.destroy()

        for entry in reversed(canvas_list):
            tk.Label(sic_frame,text=entry, font=("Calibri","13"), bg="#e9e9e9",anchor="w",width=65).grid(row=row,column=column)
            row += 1


""" Here Starts the GUI """
root = tk.Tk()
root.title("Motor control")
root.geometry("1080x600")
root.resizable(False,False)

##UI Elements
#Canvas for the serial input
serial_input_canvas = tk.Canvas(root, width=600,height=600, bg="#e1e1e1")

#Frame for the serial canvas
sic_frame = tk.LabelFrame(serial_input_canvas,text="Terminal", bg="#e9e9e9",)
serial_input_canvas.create_window((5,0),window=sic_frame,anchor="nw")

#Scrollbar for the serial input canvas
sic_scrollbar = tk.Scrollbar(root, orient="vertical", command= serial_input_canvas.yview)
serial_input_canvas.config(yscrollcommand= sic_scrollbar.set)

#Serial port select
selected_port = tk.StringVar()
selected_port.set("Select a Port")

#Serial Options Dropdown
serial_devices = tk.OptionMenu(root,selected_port,*ports)
serial_devices.config(width=18)

#Buttons for serial options
button_connect_serial = tk.Button(root,text="Connect to serial",width=18,height=1,padx=1,pady=1,bg="teal",fg="black",command=serial_Connect)
button_diconnect_serial = tk.Button(root,text="Disconnect from serial",width=18,height=1,padx=1,pady=1,bg="teal",fg="black",command=serial_Disconnect)

#Entry vars for motor params
rpm_input = tk.StringVar()
rpm_input.set(400)

steps_input = tk.StringVar()
steps_input.set(51200)

#Entry boxes for the paramitors for the motor
rpm_entry = tk.Entry(root,textvariable=rpm_input,width=24)
steps_entry = tk.Entry(root,textvariable=steps_input,width=24)

#Labels for motor prams entrys
rpm_label = tk.Label(root,text="Set you RPM",width=21,anchor="w")
steps_label = tk.Label(root,text="Steps motor steps",width=21,anchor="w")

#Buttons for the motor commands
button_move_con_ccw = tk.Button(root,text="Move Counter Clockwise",width=18,height=2,padx=2,pady=2,bg="lightgreen",fg="black",command=lambda: serial_Write(2,rpm_input.get()))
button_stop = tk.Button(root,text="STOP",width=18,height=2,padx=2,pady=2,bg="red",fg="black",command=lambda: serial_Write(1))
button_move_con_cw = tk.Button(root,text="Move Clockwise",width=18,height=2,padx=2,pady=2,bg="lightgreen",fg="black",command=lambda: serial_Write(3,rpm_input.get()))

button_move_steps_ccw = tk.Button(root,text="Move Steps CCW",width=18,height=2,padx=2,pady=2,bg="green",fg="black",command=lambda: serial_Write(4,steps_input.get()))
button_move_steps_cw = tk.Button(root,text="Move Steps CW",width=18,height=2,padx=2,pady=2,bg="green",fg="black",command=lambda: serial_Write(5,steps_input.get()))

##GUI Placement
serial_input_canvas.grid(row=0,column=0,rowspan=100)
sic_scrollbar.grid(row=0,column=1,rowspan=100, sticky="ns")

button_connect_serial.grid(row=0,column=3,padx=2,pady=2)
button_diconnect_serial.grid(row=0,column=4,padx=2,pady=2)

serial_devices.grid(row=0,column=2,padx=2,pady=2)
rpm_label.grid(row=2,column=2,padx=2,pady=2)
rpm_entry.grid(row=3,column=2,padx=2,pady=2)
steps_label.grid(row=4,column=2,padx=2,pady=2)
steps_entry.grid(row=5,column=2,padx=2,pady=2)

button_move_con_ccw.grid(row=1,column=3,padx=2,pady=2,rowspan=2)
button_stop.grid(row=3,column=3,padx=2,pady=2,rowspan=2)
button_move_con_cw.grid(row=5,column=3,padx=2,pady=2,rowspan=2)

button_move_steps_ccw.grid(row=1,column=4,padx=2,pady=2,rowspan=2)
button_move_steps_cw.grid(row=5,column=4,padx=2,pady=2,rowspan=2)

if __name__ == '__main__':
    try:
        while True:
            root.update()
            try:
                listen_Serial_port()
                refresh_serial()
            except:
                pass #Should the usb cable be disconnect or the connection lost for what ever reason, the progrom should't crash because of it.
            display_list()
            serial_input_canvas.config(scrollregion=serial_input_canvas.bbox("all"))
    except:
        print("Program closing down")